<?php get_header();?>

<div class="o-container -content">
    <div class="o-post">
<h1 class="o-post__title"><?php the_title();?></h1>
    <?php
    /* Start the Loop */
    if ( have_posts() ) : while ( have_posts() ) : the_post();
        function sup($text){
            $true = preg_replace('#(\d+)(st|th|nd|rd)#', '$1<sup class="super">$2</sup>', $text);
            return $true;
        }
        echo sup(the_content());
    endwhile; else: ?>
        <p>Not found.</p>
    <?php endif; // End of the loop.
    ?>
</div>
</div>

<?php get_footer(); ?>