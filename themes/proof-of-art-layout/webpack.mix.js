let mix = require('laravel-mix');

mix
    .options({
        processCssUrls: false,
    })
    .js('assets/js/index.js', 'dist')
    .sass('assets/scss/index.scss', 'dist/');
