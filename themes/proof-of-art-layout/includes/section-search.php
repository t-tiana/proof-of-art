<?php
/* Start the Loop */
if (have_posts()) : while (have_posts()) : the_post(); ?>
    <a href="<?php the_permalink(); ?>">
        <?php if (has_post_thumbnail($post->ID) || wp_attachment_is_image($post->ID)): ?>
            <?php $display = 'block' ?>
            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
        <?php else: ?>
            <?php $display = 'none' ?>
            <?php $image = null ?>
        <?php endif; ?>
        <div class="m-post">
            <div class="m-post__titleWrapper">
                <h4 class="m-post__title"><?php the_title(); ?></h4>
                <p class="m-post__date"><?php the_date(); ?></p>
            </div>
            <img class="m-post__img" src="<?php echo $image[0];?>" style="display: <?php echo $display?>" alt="post thumbnail">
        </div>
    </a>


<?php endwhile; else: ?>
<?php endif; // End of the loop.
?>