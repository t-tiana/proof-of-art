<?php get_header(); ?>
    <div class="o-container -content">
        <div class="o-postsContainer">
            <?php get_template_part('includes/section', 'archive'); ?>
        </div>
        <?php previous_posts_link();?>
        <?php next_posts_link();?>
    </div>
<?php get_footer(); ?>