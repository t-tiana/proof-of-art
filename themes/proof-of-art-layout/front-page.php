<?php /* Template Name: Landing page*/ ?>
<?php get_header(); ?>

    <?php
    echo do_shortcode('[smartslider3 slider="2"]');
    ?>


<section class="o-container -fullWidth o-intro">
    <div class="o-container -content o-intro__container">
        <div class="o-intro__imgBlock">
            <svg class="o-intro__imgBlock -img" width="100%" height="100%" viewBox="0 0 300 405" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M150 246.862C186.558 246.862 216.194 218.042 216.194 182.49C216.194 146.938 186.558 118.117 150 118.117C113.442 118.117 83.8057 146.938 83.8057 182.49C83.8057 218.042 113.442 246.862 150 246.862Z" fill="#33FF00"/>
                <path d="M216.194 304.555C252.752 304.555 282.389 275.734 282.389 240.182C282.389 204.63 252.752 175.81 216.194 175.81C179.636 175.81 150 204.63 150 240.182C150 275.734 179.636 304.555 216.194 304.555Z" fill="#9828BB"/>
                <path d="M165.789 391.397C202.348 391.397 231.984 362.576 231.984 327.024C231.984 291.472 202.348 262.652 165.789 262.652C129.231 262.652 99.5952 291.472 99.5952 327.024C99.5952 362.576 129.231 391.397 165.789 391.397Z" fill="#33FF00"/>
                <path d="M76.5182 141.498C113.076 141.498 142.713 112.677 142.713 77.1255C142.713 41.5736 113.076 12.7531 76.5182 12.7531C39.9601 12.7531 10.3239 41.5736 10.3239 77.1255C10.3239 112.677 39.9601 141.498 76.5182 141.498Z" fill="#33FF00"/>
                <path d="M76.5182 218.927C113.076 218.927 142.713 190.107 142.713 154.555C142.713 119.003 113.076 90.1822 76.5182 90.1822C39.9601 90.1822 10.3239 119.003 10.3239 154.555C10.3239 190.107 39.9601 218.927 76.5182 218.927Z" fill="#9828BB"/>
            </svg>
        </div>
        <div class="o-intro__txtBlock">
            <article class="o-intro__article" >
                <p class="o-intro__text">
                    <span class="o-intro__text -line">
                       NFT Contemporary Practices Centre
                    </span>
                    <span class="o-intro__text -line">
                      Welcome to blockchain technology below the focus of art!
                    </span>
                    From the perspective of art universalization and globalization in the language of new media - NFT - is the place for the emergence of new theory and practice.
                </p>
            </article>

        </div>

    </div>

</section>

<?php get_footer(); ?>