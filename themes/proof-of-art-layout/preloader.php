<?php /* Template Name: Preloader */ ?>
<!DOCTYPE html>
<html lang="en">
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Loading...</title>
  <link rel="stylesheet" href="style.css">
</head>

<body class="page-template page-template-preloader page-template-preloader-php page">
    <div class="move">
       <div id="triangle"></div>
    </div>
</body>

   
</html>
