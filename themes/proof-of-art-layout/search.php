<?php
/**
 * Template Name: Search Page
 */

?>
<?php get_header(); ?>
    <div class="o-container -content">
        <div class="o-postsContainer">
            <?php get_template_part('includes/section', 'search'); ?>
        </div>
    </div>
<?php get_footer(); ?>