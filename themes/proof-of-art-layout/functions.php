<?php
// add_action('template_redirect', 'add_content_security_policy');
// function add_content_security_policy()
// {
//     $csp = "default-src 'self' data: *.fontawesome.com *.klaviyo.com telemetrics.klaviyo.com/v1/metric; script-src 'self' 'unsafe-inline' *.w.org www.w3.org *.fontawesome.com *.klaviyo.com telemetrics.klaviyo.com/v1/metric; img-src * *.w3.org *.klaviyo.com; style-src 'self' 'unsafe-inline' *.fontawesome.com *.w3.org fonts.googleapis.com *.w.org *.klaviyo.com; frame-src 'self' www.youtube.com *.klaviyo.com telemetrics.klaviyo.com/v1/metric";
//     header('Content-Security-Policy: ' . $csp);
// }

/*Activate WordPress Maintenance Mode/
 function wp_maintenance_mode() {
    if ( !is_user_logged_in() ) {
        wp_die('<h1>Coming soon...</h1><br /> Check back later.');
    }
}
add_action('get_header', 'wp_maintenance_mode');

*/
define('plugin_path', TEMPLATEPATH . '/plugins/');
define('plugin_path_url', get_template_directory_uri() . '/plugins/');


add_theme_support( 'post-thumbnails' );

register_nav_menus(array(
    'main_menu' => 'Main menu',
));



add_action('wp_default_scripts', function ($scripts) {
    if (!empty($scripts->registered['jquery'])) {
        $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, ['jquery-migrate']);
    }
});

function connect()
{
    wp_register_style('main', get_template_directory_uri() . '/dist/index.css', [], 1, 'all');
    wp_enqueue_style('main');

    wp_enqueue_script('jquery');

    wp_register_script('main', get_template_directory_uri() . '/dist/index.js', [], 1, true);
    wp_enqueue_script('main');

    if (!defined('THEME_IMG_PATH')) {
        define('THEME_IMG_PATH', get_stylesheet_directory_uri() . '/dist/img');
    }
}
add_action('wp_enqueue_scripts', 'connect');


function custom_body_classes($classes)
{
    if (is_home() || is_singular('post') || is_category() || is_tag() || is_tax() || is_page_template('front-page.php')) {
        $classes[] = 'front-page';
    } elseif (is_home() || is_singular('post') || is_category() || is_tag() || is_tax() || is_page_template('about.php')) {
        $classes[] = 'about';
    }
    return $classes;
}
add_filter('body_class', 'custom_body_classes');
//


function remove_admin_login_header()
{
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

add_filter('show_admin_bar', '__return_false');
