<?php get_header(); ?>

<aside class="sidebar">
	<?php if ( !function_exists('dynamic_sidebar')
        || !dynamic_sidebar(2) ) : ?>
	<?php endif; ?>
</aside>
<main class="content">
		<h1 class="post-title">Error 404 - Страница не найдена</h1>
	    <p>Извините, но того что Вы искали здесь нет.</p>
</main>

<?php get_footer(); ?>